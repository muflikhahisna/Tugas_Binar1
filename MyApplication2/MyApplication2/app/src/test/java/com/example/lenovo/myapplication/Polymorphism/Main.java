package com.example.lenovo.myapplication.Polymorphism;

import org.junit.Test;

/**
 * Created by LENOVO on 5/3/2018.
 */

public class Main {
    @Test
    public void addition_isCorrect() throws Exception {

//        MessageSender messageSender = new MessageSender ();
//        messageSender.setFrom("ali");
//        messageSender.setTo("ismail");
//        messageSender.setMsg("hi bro!");
//
//        messageSender.SendMessage();
//
//        MessageSender sender = new SMSSender("b", "a", "oke");
//        MessageSender smssender = new EmailSender("a@gmail.com","b.gmail.com","haiii");
//


        MessageSender[] messageSenders = new MessageSender[2];
        messageSenders[0] = new SMSSender("081234", "089765", "hey");
        messageSenders[1] = new EmailSender("081321", "kak@gmail.com", "hello");

        for (int i = 0; i < messageSenders.length; i++){
            messageSenders[i].SendMessage();
        }
    }
}
