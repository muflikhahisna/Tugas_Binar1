package com.example.lenovo.myapplication.Polymorphism;

/**
 * Created by LENOVO on 5/3/2018.
 */

public class EmailSender extends MessageSender {
    private String subject;

    public EmailSender(String to, String from, String msg, String subject) {
        super(to, from, msg);
        this.subject = subject;
    }

    public EmailSender(String to, String from, String msg) {
        super(to, from, msg);
    }

    @Override //mendklarasikan lg method yang ada idperennya sama persis
    //overload : membuat method yang sm tp beda paramaternya
    void SendMessage() {
        System.out.println(String.format("Mengirim Email dari %s ke %s. Email : %s", getFrom(), getTo(), getMsg(), subject, getMsg()));
    }
}
