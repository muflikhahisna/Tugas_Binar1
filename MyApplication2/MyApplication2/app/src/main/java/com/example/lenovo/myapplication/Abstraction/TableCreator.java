package com.example.lenovo.myapplication.Abstraction;

/**
 * Created by LENOVO on 5/3/2018.
 */

public abstract class TableCreator {

    void drawTable() {
        //menetukan jumlah baris
        int jmlBaris = getJmlBaris();
        System.out.println("jumlah baris : " + jmlBaris);

        //menentukan jl kolom
        int jmlKolom = getJmlKolom();

        buatFileExcel();
        butSheet();
        //menemtukan font

        //menentukan fontsize, type, warna
        for (int i = 0; i < jmlBaris; i++) {
            for(int j = 0; j < jmlKolom; j++){
                //menentukan fz,ype, warna
                TableProperties tp = getTableProperties(i , j);
                drawtTablePerPixel(i,j,tp);
            }
        }

    }

    void drawtTablePerPixel(int row, int col, TableProperties tp) {
        System.out.println(String.format("Menggambbar table Baris %d Kolom %d." +
        "Type : %s | Warna : %s | Size : %d", col, row, tp.getType(), tp.getWarna(), tp.getFontsize() ));
    }

    protected abstract TableProperties getTableProperties(int i, int j);

    private void butSheet() {
    }

    private void buatFileExcel() {
    }

    protected abstract int getJmlKolom();

    protected abstract int getJmlBaris();


}
