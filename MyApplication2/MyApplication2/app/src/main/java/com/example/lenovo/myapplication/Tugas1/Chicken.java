package com.example.lenovo.myapplication.Tugas1;

/**
 * Created by LENOVO on 5/8/2018.
 */

public class Chicken extends Animal {
    Chicken() {
        System.out.println("Cat has 2 legs");
    }
    public Chicken() {
        super();
    }

    public Chicken(int kaki, String alatNafas) {
        super(kaki, alatNafas);
    }

    @Override
    public int getKaki() {
        return super.getKaki();
    }

    @Override
    public void setKaki(int kaki) {
        super.setKaki(kaki);
    }

    @Override
    public String getAlatNafas() {
        return super.getAlatNafas();
    }

    @Override
    public void setAlatNafas(String alatNafas) {
        super.setAlatNafas(alatNafas);
    }
}
