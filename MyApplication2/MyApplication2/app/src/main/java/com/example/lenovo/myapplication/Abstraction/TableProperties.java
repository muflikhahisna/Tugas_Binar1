package com.example.lenovo.myapplication.Abstraction;

/**
 * Created by LENOVO on 5/3/2018.
 */

public class TableProperties {
    private String warna;
    private String type;
    private int fontsize;

    public TableProperties(String warna, String type, int fontsize) {
        this.warna = warna;
        this.type = type;
        this.fontsize = fontsize;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getFontsize() {
        return fontsize;
    }

    public void setFontsize(int fontsize) {
        this.fontsize = fontsize;
    }
}
