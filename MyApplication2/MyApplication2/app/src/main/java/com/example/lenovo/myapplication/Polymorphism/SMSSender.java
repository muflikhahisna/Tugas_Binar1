package com.example.lenovo.myapplication.Polymorphism;

/**
 * Created by LENOVO on 5/3/2018.
 */

public class SMSSender extends MessageSender{
    public SMSSender(String to, String from, String msg) {
        super(to, from, msg);
    }

    void SendMessage() {
        System.out.println(String.format("Mengirim SMS dari %s ke %s. SMS : %s", getFrom(), getTo(), getMsg()));
    }
}
