package com.example.lenovo.myapplication.Tugas1;

/**
 * Created by LENOVO on 5/8/2018.
 */

public class Animal {
    private int Kaki;
    private String AlatNafas;

    public Animal(){}

    public Animal(int kaki, String alatNafas) {
        Kaki = kaki;
        AlatNafas = alatNafas;
    }

    public int getKaki() {
        return Kaki;
    }

    public void setKaki(int kaki) {
        Kaki = kaki;
    }

    public String getAlatNafas() {
        return AlatNafas;
    }

    public void setAlatNafas(String alatNafas) {
        AlatNafas = alatNafas;
    }
}
