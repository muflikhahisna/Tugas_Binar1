package com.example.lenovo.myapplication.Abstraction;

/**
 * Created by LENOVO on 5/3/2018.
 */

public class TableKependudukan extends TableCreator {
    @Override
    void drawTable() {
        super.drawTable();
    }

    @Override
    void drawtTablePerPixel(int row, int col, TableProperties tp) {
        super.drawtTablePerPixel(row, col, tp);
    }

    @Override
    protected TableProperties getTableProperties(int row, int j) {
        if (row == 0){
            return new TableProperties("hitam", "bold", 14);
        }

        return new TableProperties("hitam", "regular", 12);
    }

    @Override
    protected int getJmlBaris() {
        return 5;
    }

    @Override
    protected int getJmlKolom() {
        return 3;
    }


}
